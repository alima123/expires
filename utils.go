package expires

func (cm *CacheOpMap) CacheOperation(op string, key interface{}, opFunc func() DataExpires) (data DataExpires) {
	var (
		cache = cm.LazyInitCachePoolOp(op)
		ok    bool
	)
	data, ok = cache.Load(key)
	if !ok {
		data = opFunc()
		if data != nil {
			cache.Store(key, data)
		}
		return
	}

	return
}

func (cm *CacheOpMap) CacheOperationWithError(op string, key interface{}, opFunc func() (DataExpires, error)) (data DataExpires, err error) {
	var (
		cache = cm.LazyInitCachePoolOp(op)
		ok    bool
	)
	data, ok = cache.Load(key)
	if !ok {
		data, err = opFunc()
		if err != nil {
			return
		}
		cache.Store(key, data)
	}
	return
}
