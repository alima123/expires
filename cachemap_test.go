package expires

import (
	"fmt"
	"testing"
	"time"
)

func TestCacheMapDataExpires(t *testing.T) {
	cm := CacheOpMap{}
	cache := cm.LazyInitCachePoolOp("op")
	cache.Store("key_1", NewDataExpires("value_1", 1*time.Second))

	time.Sleep(1 * time.Second)
	data, ok := cache.Load("key_1")
	if !ok {
		t.FailNow()
	}
	fmt.Printf("data: %s\n", data.Data())
}

func TestCacheOperation(t *testing.T) {
	cm := CacheOpMap{}
	data := cm.CacheOperation("op", "key_1", func() DataExpires {
		return NewDataExpires("value_1", 1*time.Second)
	})
	fmt.Printf("data: %s\n", data.Data())

	newData := cm.CacheOperation("op", "key_1", func() DataExpires {
		return NewDataExpires("value_3", 1*time.Second)
	})
	if data != newData {
		t.FailNow()
	}
	fmt.Printf("data: %s\n", data.Data())
}
